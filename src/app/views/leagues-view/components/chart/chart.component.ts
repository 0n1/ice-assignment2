import { Component, OnInit, OnChanges, Input } from '@angular/core';

import { Router } from "@angular/router";

import { Config } from 'config';

import * as Chartist from 'chartist';

declare var require: any;

@Component({
    selector: 'app-chart',
    templateUrl: './chart.component.html',
    styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
    @Input() team: any;
    @Input() standing: any;
    @Input() league: any;

    public eachTeamGame: any;

    public lineChartType;
    public gradientStroke;
    public chartColor;
    public canvas: any;
    public ctx;
    public gradientFill;
    public lineChartData: Array<any>;
    public lineChartOptions: any;
    public lineChartLabels: Array<any>;
    public lineChartColors: Array<any>

    constructor(private config: Config, private router: Router) { }

    // events
    public chartClicked(e: any): void {
        if (e.active.length) {
            const { _index: index } = e.active[0]
            const id = this.config.getID(this.eachTeamGame[index]._links.self.href);
            this.router.navigate([`fixtures/${id}`])
        }
    }

    public hexToRGB(hex, alpha) {
        var r = parseInt(hex.slice(1, 3), 16),
            g = parseInt(hex.slice(3, 5), 16),
            b = parseInt(hex.slice(5, 7), 16);

        if (alpha) {
            return "rgba(" + r + ", " + g + ", " + b + ", " + alpha + ")";
        } else {
            return "rgb(" + r + ", " + g + ", " + b + ")";
        }
    }

    ngOnInit() {
        this.chartColor = "#FFFFFF";
        this.canvas = document.getElementById("chart-canvas");
        this.ctx = this.canvas.getContext("2d");

        this.gradientStroke = this.ctx.createLinearGradient(500, 0, 100, 0);
        this.gradientStroke.addColorStop(0, '#80b6f4');
        this.gradientStroke.addColorStop(1, this.chartColor);

        this.gradientFill = this.ctx.createLinearGradient(0, 200, 0, 50);
        this.gradientFill.addColorStop(0, "rgba(128, 182, 244, 0)");
        this.gradientFill.addColorStop(1, "rgba(255, 255, 255, 0.24)");

        const { getID } = this.config;

        let playedGamesArray = Array.from(Array(this.standing.playedGames + 1).keys())
        playedGamesArray.shift()

        let competitionID = getID(this.league._links.competition.href);

        this.eachTeamGame = this.team.fixtures.map(fixture => {
            if (competitionID === getID(fixture._links.competition.href))
                return fixture;
        }).filter((element) => element !== undefined);
        this.eachTeamGame = this.eachTeamGame.sort((a, b) => a.matchday - b.matchday)

        let scoreOfTeamEachDay = this.eachTeamGame.map(fixture => {
            if (this.standing.teamName === fixture.awayTeamName)
                return fixture.result.goalsAwayTeam
            else
                return fixture.result.goalsHomeTeam
        })

        this.lineChartData = [
            {
                label: "Goals",

                pointBorderWidth: 1,
                pointHoverRadius: 7,
                pointHoverBorderWidth: 2,
                pointRadius: 5,
                fill: true,

                borderWidth: 2,
                data: scoreOfTeamEachDay
            }
        ];
        this.lineChartColors = [
            {
                backgroundColor: this.gradientFill,
                borderColor: this.chartColor,
                pointBorderColor: this.chartColor,
                pointBackgroundColor: "#2c2c2c",
                pointHoverBackgroundColor: "#2c2c2c",
                pointHoverBorderColor: this.chartColor,
            }
        ];
        this.lineChartLabels = playedGamesArray;

        this.lineChartOptions = {
            layout: {
                padding: {
                    left: 20,
                    right: 20,
                    top: 0,
                    bottom: 0
                }
            },
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: '#fff',
                titleFontColor: '#333',
                bodyFontColor: '#666',
                bodySpacing: 4,
                xPadding: 12,
                mode: "nearest",
                intersect: 0,
                position: "nearest",
                callbacks: {
                    label: (tooltipItem, data) => {
                        let activeLabel = tooltipItem.index;

                        const gameFixtures = this.eachTeamGame[activeLabel]
                        const { homeTeamName, awayTeamName } = gameFixtures;
                        const { goalsHomeTeam, goalsAwayTeam } = gameFixtures.result;

                        return [`${goalsHomeTeam} - ${homeTeamName}`, `${goalsAwayTeam} - ${awayTeamName}`];
                    },
                    title: function(tooltipItems, data) {
                        return '';
                        // let activeLabel = tooltipItems.length && tooltipItems[0].index;
                        // if (!tooltipItems.length || activeLabel === -1) return '';
                        //
                        // const gameFixtures = this.eachTeamGame[activeLabel];
                        // const { date, homeTeamName, awayTeamName } = gameFixtures;
                        // const { goalsHomeTeam, goalsAwayTeam } = gameFixtures.result;
                        //
                        // return [`${date}`];
                    }
                }
            },
            legend: {
                position: "bottom",
                fillStyle: "#FFF",
                display: false
            },
            scales: {
                yAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: "Goals",
                        fontColor: "rgba(255,255,255,0.8)"

                    },
                    ticks: {
                        fontColor: "rgba(255,255,255,0.4)",
                        fontStyle: "bold",
                        beginAtZero: true,
                        maxTicksLimit: 5,
                        padding: 10
                    },
                    gridLines: {
                        drawTicks: true,
                        drawBorder: false,
                        display: true,
                        color: "rgba(255,255,255,0.1)",
                        zeroLineColor: "transparent"
                    }

                }],
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: "Matches Played",
                        fontColor: "rgba(255,255,255,0.8)"
                    },
                    gridLines: {
                        zeroLineColor: "transparent",
                        display: false,

                    },
                    ticks: {
                        padding: 10,
                        fontColor: "rgba(255,255,255,0.4)",
                        fontStyle: "bold"
                    }
                }]
            }
        };

        this.lineChartType = 'line';
    }
}
