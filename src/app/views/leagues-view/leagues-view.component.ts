import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Config } from 'config';

@Component({
    selector: 'app-leagues-view',
    templateUrl: './leagues-view.component.html',
    styleUrls: ['./leagues-view.component.scss']
})
export class LeaguesViewComponent implements OnInit {
    league: any = {};
    graphSelected: number[] = [];
    teams: any = {};

    constructor(private http: HttpClient, private config: Config) { }

    ngOnInit(): void {
        this.http.get('competitions/445/leagueTable').subscribe((res: any) => this.league = res);

        // default first for testing
        // this.http.get(`teams/65/fixtures`).subscribe((res: any) => { this.teams[0] = res; this.graphSelected.push(0) });
    }


    toggleGraph(index): void {
        let graphSelectedIndex = this.graphSelected.indexOf(index);
        if (graphSelectedIndex > -1)
            this.graphSelected.splice(graphSelectedIndex, 1)
        else
            this.graphSelected.push(index)

        let id = this.league.standing[index]._links.team.href.split('/');
        id = id[id.length - 1];

        if (id && !this.teams[index]) {
            this.teams[index] = {};
            this.http.get(`teams/${id}/fixtures`).subscribe((res: any) => this.teams[index] = res);
        }
    }
}
