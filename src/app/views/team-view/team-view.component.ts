import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { HttpClient } from '@angular/common/http';
import { Config } from 'config';

@Component({
    selector: 'app-team-view',
    templateUrl: './team-view.component.html',
    styleUrls: ['./team-view.component.scss']
})
export class TeamViewComponent implements OnInit {
    team: any = {};
    players: any = {};
    error: any = {};

    constructor(
        private route: ActivatedRoute,
        private http: HttpClient,
        private config: Config,
        private location: Location) { }

    ngOnInit(): void {
        const id = +this.route.snapshot.paramMap.get('id');

        this.http.get(`teams/${id}`).subscribe((res: any) => this.team = { ...this.team, ...res }, (error: any) => this.error = error);
        this.http.get(`teams/${id}/players`).subscribe((res: any) => this.team.players = res.players, (error: any) => this.error = error);
    }

}
