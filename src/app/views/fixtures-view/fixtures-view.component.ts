import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { HttpClient } from '@angular/common/http';
import { Config } from 'config';

@Component({
    selector: 'app-fixtures-view',
    templateUrl: './fixtures-view.component.html',
    styleUrls: ['./fixtures-view.component.scss']
})
export class FixturesViewComponent implements OnInit {
    team: any = {};
    fixture: any = {};
    vsImage: string = '';
    error: any = {};

    constructor(
        private route: ActivatedRoute,
        private http: HttpClient,
        private config: Config,
        private location: Location) { }

    ngOnInit(): void {
        const id = +this.route.snapshot.paramMap.get('id');

        // this.http.get(`teams/${id}`).subscribe((res: any) => this.team = { ...this.team, ...res }, (error: any) => this.error = error);
        this.vsImage = '/assets/images/vs.png'

        this.http.get(`fixtures/${id}/`).subscribe((res: any) => {
            this.fixture = res.fixture;
            // this.fixtues = res.fixtues;
        }, (error: any) => this.error = error);
    }

}
