import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Config } from 'config';

@Injectable()
export class FootballApiInterceptor implements HttpInterceptor {
    constructor (private config: Config){
        this.config = config;
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authReq = req.clone({
            url: `${this.config.api}/${req.url}`,
            headers: req.headers.set('X-Auth-Token', '80dc284b8d924fd299620dd1261f6e89')
        });
        return next.handle(authReq);
    }
}
