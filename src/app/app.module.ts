import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { FootballApiInterceptor } from './interceptors/football-api.interceptor';

import { Config } from 'config';

import { AppComponent } from './app.component';
import { LogoComponent } from './components/logo/logo.component';
import { NavbarComponent } from './components/navbar/navbar.component';

import { RouterModule, Routes } from '@angular/router';
import { LeaguesViewComponent } from './views/leagues-view/leagues-view.component';
import { TeamViewComponent } from './views/team-view/team-view.component';
import { PageNotFoundViewComponent } from './views/page-not-found-view/page-not-found-view.component';
import { LoaderComponent } from './components/loader/loader.component';

import { ChartsModule } from 'ng2-charts';
import { ChartComponent } from './views/leagues-view/components/chart/chart.component';
import { LoaderSoccerComponent } from './components/loader-soccer/loader-soccer.component';
import { LeagueStandingPipe } from './pipes/league-standing.pipe';
import { FixturesViewComponent } from './views/fixtures-view/fixtures-view.component';
import { FooterComponent } from './components/footer/footer.component';

const appRoutes: Routes = [
    { path: '', component: LeaguesViewComponent },
    { path: 'teams/:id', component: TeamViewComponent },
    { path: 'fixtures/:id', component: FixturesViewComponent },
    { path: '**', component: PageNotFoundViewComponent }
];

@NgModule({
    declarations: [
        AppComponent,
        LogoComponent,
        NavbarComponent,
        LeaguesViewComponent,
        PageNotFoundViewComponent,
        LoaderComponent,
        TeamViewComponent,
        ChartComponent,
        LoaderSoccerComponent,
        LeagueStandingPipe,
        FixturesViewComponent,
        FooterComponent
    ],
    imports: [
        ChartsModule,
        NgbModule.forRoot(),
        RouterModule.forRoot(
            appRoutes,
            { enableTracing: false } // <-- debugging purposes only
        ),
        BrowserModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: FootballApiInterceptor,
            multi: true
        },
        Config
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
