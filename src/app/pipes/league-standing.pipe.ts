import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'leagueStanding'
})
export class LeagueStandingPipe implements PipeTransform {
    transform(items: any[], searchLeagueStanding: string): any[] {
        if (!items) return [];
        if (!searchLeagueStanding) return items;
        searchLeagueStanding = searchLeagueStanding.toLowerCase();
        return items.filter(item => {
            return item.teamName.toLowerCase().includes(searchLeagueStanding);
        });
    }
}
