import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoaderSoccerComponent } from './loader-soccer.component';

describe('LoaderSoccerComponent', () => {
  let component: LoaderSoccerComponent;
  let fixture: ComponentFixture<LoaderSoccerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoaderSoccerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderSoccerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
