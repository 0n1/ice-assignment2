import { Injectable, isDevMode } from '@angular/core';

@Injectable()
export class Config {
    api: string = isDevMode() ? 'http://api.football-data.org/v1' : 'https://api.football-data.org/v1';
    removeApiUrl: Function = (url) => {
        url = !isDevMode() ? url.replace('http://', 'https://') : url;
        return url.replace(this.api, '')
    };
    getID: Function = (url) => {
        url = !isDevMode() ? url.replace('http://', 'https://') : url;

        let slash = url.split('?')[0].split('/');
        return slash[slash.length - 1]
    };
}
